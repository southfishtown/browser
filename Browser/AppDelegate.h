//
//  AppDelegate.h
//  Browser
//
//  Created by Pu Zhao on 2019/10/23.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

